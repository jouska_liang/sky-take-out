package com.sky.config;

import com.sky.properties.AliOssProperties;
import com.sky.utils.AliOssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * OSS 服务配置类,用于创建aliOssUtil对象
 */
@Configuration
@Slf4j
public class OssConfiguration {

    @Bean
    @ConditionalOnMissingBean   // 当指定的 Bean 不存在时，才执行所注解的配置或装配
    public AliOssUtil aliOssUtil(AliOssProperties properties) {
        String endpoint = properties.getEndpoint();
        String accessKeyId = properties.getAccessKeyId();
        String accessKeySecret = properties.getAccessKeySecret();
        String bucketName = properties.getBucketName();

        log.info("开始创建阿里云文件上传工具类对象：{}",properties);
        return new AliOssUtil(endpoint,accessKeyId,accessKeySecret,bucketName);
    }
}
