package com.sky.aspect;

import com.sky.annotation.AutoFill;
import com.sky.constant.AutoFillConstant;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

/**
 * 自定义切面，实现公共字段自动填充处理逻辑
 */
@Aspect
@Component
@Slf4j
public class AutoFillAspect {

    /**
     * 切入点
     */
    @Pointcut("execution(* com.sky.mapper.*.*(..)) && @annotation(com.sky.annotation.AutoFill)")
    public void autoFillPointCut() {

    }

    /**
     * 前置通知，在通知中对公共字段赋值
     */
    @Before("autoFillPointCut()")
    public void autoFill(JoinPoint joinPoint) {
        log.info("开始公共字段填充...");

        // 获取当前拦截方法的的数据库操作类型
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();     // 方法签名对象
        AutoFill autoFill = signature.getMethod().getAnnotation(AutoFill.class);    // 获得方法上的注解对象
        if (autoFill == null) return;

        OperationType operationType = autoFill.value();     // 获得数据库操作类型
        Object[] args = joinPoint.getArgs();    // 获取当前被拦截方法的参数--实体对象
        if (args.length == 0) return;

        Object entity = args[0];

        // 准备赋值的数据
        LocalDateTime now = LocalDateTime.now();
        Long currentId = BaseContext.getCurrentId();

        // 为公共字段赋值
        try {
            if (operationType == OperationType.INSERT) {
                fillCommonFields(entity, now, currentId, true);
            } else if (operationType == OperationType.UPDATE) {
                fillCommonFields(entity, now, currentId, false);
            }
        } catch (Exception e) {
            log.error("公共字段赋值失败...", e);
        }
    }

    /**
     * 为公共字段赋值
     * @param entity 需要操作的对象实例，即将调用其方法的那个实体
     * @param now 当前时间
     * @param currentId 操作人id
     * @param isInsert 是否为insert
     * @throws NoSuchMethodException 在尝试使用反射调用方法时未找到指定的方法
     * @throws IllegalAccessException 受检异常，表示 Java 反射模块在尝试访问类的字段、调用方法或构造函数时，未能成功访问相关成员
     * @throws InvocationTargetException 非受检异常，表示在使用 Java 反射调用方法时，目标方法本身抛出了一个异常
     */
    private void fillCommonFields(Object entity, LocalDateTime now, Long currentId, boolean isInsert) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        fillField(entity, AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class, now);
        fillField(entity, AutoFillConstant.SET_UPDATE_USER, Long.class, currentId);

        if (isInsert) {
            fillField(entity, AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class, now);
            fillField(entity, AutoFillConstant.SET_CREATE_USER, Long.class, currentId);
        }
    }

    /**
     * 通过反射机制来调用一个对象的某个方法，并向该方法传递一个值。这通常用于设置对象的私有字段值
     * @param entity 需要操作的对象实例，即将调用其方法的那个实体
     * @param methodName 需要调用的方法的名称
     * @param fieldType 方法参数的类型
     * @param value 要设置到对象的方法参数
     * @throws NoSuchMethodException  如果找不到与名称和参数类型对应的方法，则抛出此异常
     * @throws SecurityException 当存在安全违规，比如违反 Java 安全策略时，抛出此异常
     * @throws IllegalAccessException 当没有权限访问特定方法时抛出此异常
     * @throws IllegalArgumentException 当向方法传递了一个不合法或不适当的参数时
     * @throws InvocationTargetException 如果反射调用的目标方法本身抛出异常时
     */
    private void fillField(Object entity, String methodName, Class<?> fieldType, Object value) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Method method = getAccessibleMethod(entity.getClass(), methodName, fieldType);
        method.invoke(entity, value);
    }

    /**
     * 通过反射机制在给定的类（Class<?> clazz）中获取指定名称（String methodName）和参数类型（Class<?> fieldType）的方法，并确保这个方法是可访问的
     * @param clazz 想要查找方法的类的 Class 对象
     * @param methodName 需要查找的方法的名称
     * @param fieldType 方法的参数类型，帮助在有重载方法的情况下区分所需方法
     * @return *
     * @throws NoSuchMethodException 如果在类中没有找到符合指定名称和参数类型的方法，将抛出此异常
     */
    private Method getAccessibleMethod(Class<?> clazz, String methodName, Class<?> fieldType) throws NoSuchMethodException {
        try {
            Method method = clazz.getDeclaredMethod(methodName, fieldType);
            method.setAccessible(true);
            return method;
        } catch (NoSuchMethodException e) {
            log.warn("方法未找到: " + methodName);
            throw e;
        }
    }
}
